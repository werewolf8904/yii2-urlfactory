<?php

namespace werewolf8904\urlfactory;


use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class ObjectUrlFactory implements IObjectUrlFactory
{
    public $map;

    /**
     * ObjectUrlFactory constructor.
     *
     * @param null|array $map
     */
    public function __construct($map = null)
    {
        $this->map = $map;
    }

    /**
     * @param object $model
     * @param bool   $scheme
     *
     * @return string
     */
    public function getUrl(object $model, $scheme = false)
    {
        return $this->getUrlInternal(\get_class($model), ArrayHelper::getValue($model, 'attributes', []), $scheme);
    }

    /**
     * @param      $key
     * @param      $array
     * @param bool $scheme
     *
     * @return string
     */
    protected function getUrlInternal($key, $array, $scheme = false)
    {
        if (\is_array($array) && ($map = ArrayHelper::getValue($this->map, $key, false))) {
            $route = array_shift($map);
            $map = array_intersect_key($array, array_flip($map));
            $map[0] = $route;
            return Url::toRoute($map, $scheme);
        }
        return Url::toRoute(['/'], $scheme);
    }

    /**
     * @param            $key
     * @param array|null $array
     * @param bool       $scheme
     *
     * @return string
     */
    public function getUrlFromArray($key, array $array = null, $scheme = false)
    {
        return $this->getUrlInternal($key, $array, $scheme);
    }
}