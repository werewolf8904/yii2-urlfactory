<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 15.08.2018
 * Time: 11:36
 */

namespace werewolf8904\urlfactory;

interface IObjectUrlFactory
{
    public function getUrl(object $model, $scheme = false);

    public function getUrlFromArray($key, array $array = [], $scheme = false);
}